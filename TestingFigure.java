package TestingFigureAnimal;

import FigureSquareCircle.FigureCircle;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestingFigure {

   @Test(priority = 1)
    public void figureCircleSquarePositive(){
       FigureCircle figureCircle = new FigureCircle();
       Assert.assertEquals(figureCircle.calcSquare(7), 153);
       System.out.println("Square is correct");

   }
    @Test(priority = 2)
    public void figureCircleSquareFalse(){
        int radius = 9;
        FigureCircle figureCircle = new FigureCircle();
           try {
        Assert.assertEquals(figureCircle.calcSquare(radius), 201);
}         catch (AssertionError ex){
        ex.printStackTrace();
        System.out.println("Square is not correct");
}
    }

    @Test(priority = 3)
    public void figureCircleFalseText(){
        String radius = "Test";
        FigureCircle figureCircle = new FigureCircle();
        if(radius =="Test"){
            throw new AssertionError("Incorrect characters");}
    }

    @Test(priority = 4)
    public void figureCircleFalseMinus(){
    int radius = -7;
     FigureCircle figureCircle = new FigureCircle();
     if(radius ==-7){
        throw new AssertionError("Incorrect character");}
    }


}



