package TestingFigureAnimal;

import Sound.AnimalSound;
import Sound.CatSound;

import Sound.DogSound;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestingAnimalSound {

    @Test
    public void testAnimalSoundCat() {

        CatSound catSound1 = new CatSound();

     Assert.assertEquals(catSound1.ShowSound(), "A cat says Meow-Meow");
     System.out.println("Great!");

        }


    @Test
    public void testAnimalSoundDog() {

        DogSound dogSound1 = new DogSound();

        Assert.assertTrue(dogSound1.ShowSound().equals("A dog says bow-wow-wow"));
        System.out.println("Good!");

    }

}

